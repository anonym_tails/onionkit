import os
from pathlib import Path
import shutil
import abc
from colorlog import getLogger
from contextlib import contextmanager
from collections import OrderedDict

from onionkit import _
from onionkit import DEBUG
from onionkit.dbus.object import DBusObject
from onionkit.service_status import Status
from onionkit.mountpoint import MountPoint
from onionkit.data_file import DataFile

from onionkit.data_files.options_file import OptionsFile
from onionkit.data_files.hs_private_key import HSPrivateKey

from onionkit.options.virtual_port import VirtualPort
from onionkit.options.persistence import Persistence
from onionkit.options.autostart import Autostart
from onionkit.options.allow_localhost import AllowLocalhost
from onionkit.options.allow_lan import AllowLAN

from onionkit.exceptions import ServiceNotInstalledError
from onionkit.exceptions import ServiceAlreadyStartedError
from onionkit.exceptions import MonitorError

from onionkit.systemd import SystemdManager
from onionkit.tor import TorManager
from onionkit.container import ContainerManager

# Only required for type hints
from typing import TYPE_CHECKING, List, Dict
if TYPE_CHECKING:
    from onionkit.option import OnionServiceOption

from onionkit import STATE_DIR, TEMP_STATE_DIR, SERVICES_PATH

logger = getLogger(__name__)


class OnionService(DBusObject, metaclass=abc.ABCMeta):

    # XXX: decide whether to remove IsInstalled, IsRunning, IsPublished
    dbus_info = '''
<node>
    <interface name='org.boum.tails.OnionKit.Service'>
        <method name='Install'/>
        <method name='Uninstall'/>
        <method name='Start'/>
        <method name='Stop'/>
        <method name='RegenerateAddress'/>
        <property name="Options" type="ao" access="read">
            <annotation name="org.freedesktop.DBus.Property.EmitsChangedSignal" value="true"/>
        </property>
        <property name="Status" type="s" access="read">
            <annotation name="org.freedesktop.DBus.Property.EmitsChangedSignal" value="true"/>
        </property>
        <property name="TransactionStatus" type="s" access="read">
            <annotation name="org.freedesktop.DBus.Property.EmitsChangedSignal" value="true"/>
        </property>
        <property name="TransactionProgress" type="i" access="read">
            <annotation name="org.freedesktop.DBus.Property.EmitsChangedSignal" value="true"/>
        </property>
        <property name="Address" type="s" access="read">
            <annotation name="org.freedesktop.DBus.Property.EmitsChangedSignal" value="true"/>
        </property>
        <property name="IsInstalled" type="b" access="read">
            <annotation name="org.freedesktop.DBus.Property.EmitsChangedSignal" value="true"/>
        </property>
        <property name="IsRunning" type="b" access="read"/>
        <property name="IsPublished" type="b" access="read"/>
        <property name="ConnectionInfo" type="s" access="read"/>        
        <property name="Name" type="s" access="read"/>
        <property name="NameForDisplay" type="s" access="read"/>
        <property name="Description" type="s" access="read"/>
        <property name="DescriptionLong" type="s" access="read"/>
        <property name="DocumentationURL" type="s" access="read"/>
        <property name="Icon" type="s" access="read"/>
        <property name="ClientApplication" type="s" access="read"/>
        <property name="ClientApplicationForDisplay" type="s" access="read"/>
    </interface>
</node>
    '''

    @property
    def dbus_path(self):
        return os.path.join(SERVICES_PATH, self.Name)

    def __init__(self):
        super().__init__()
        logger.debug("Initializing service %r", self.Name)

        if Path(STATE_DIR, self.Name).exists():
            self.state_dir = Path(STATE_DIR, self.Name)
        else:
            self.state_dir = Path(TEMP_STATE_DIR, self.Name)
        self.is_installed_file = Path(self.state_dir, ".is_installed")
        self.options_dict = OrderedDict()  # type: Dict[str, OnionServiceOption]
        self.options_file = OptionsFile()
        self.private_key = HSPrivateKey()
        self.data_files += [self.options_file, self.private_key]
        self.mount_points = [f for f in self.data_files if isinstance(f, MountPoint)]
        self.expand_data_file_paths()

        self._status = Status.INITIALIZING
        self._transaction_status = str()
        self._transaction_progress = 101  # 101 means the progress cannot be calculated
        self._is_installed = self.is_installed_file.exists()
        self._address = self.private_key.derive_onion_address() if self.IsInstalled else str()

        self.tor = TorManager(self.update_transaction_progress)
        self.systemd = SystemdManager(self.Name, self.systemd_service, self.update_status)
        self.container = ContainerManager(self.Name, self.state_dir, self.update_transaction_status,
                                          self.update_transaction_progress)

        self.set_initial_status()

        try:
            if self.container.is_running():
                self.systemd.start_monitoring()
        except MonitorError as e:
            # Don't abort initialization if monitoring fails, but set an error status
            self.Status = Status.ERROR
            logger.exception(e)

        if self.IsInstalled:
            self.initialize_options()

    # ----- Exported functions ----- #

    def Install(self):
        logger.info("Installing service %r", self.Name)
        self.Status = Status.INSTALLING
        try:
            self.TransactionStatus = _("Creating container")
            self.container.create()

            self.TransactionStatus = _("Starting container for setup")
            with self.container.run():

                with self.prevent_service_autostart():
                    self.container.install_packages(self.packages)

                self.TransactionStatus = _("Configuring service")
                self.configure_top()

                self.create_state_dir()
                self.create_data_files()
                self.Address = self.private_key.derive_onion_address()
                self.initialize_options()

                # Some services configure files in the mount points, so mount them before running configure_bottom()
                self.mount()

                self.configure_bottom()

            self.Status = Status.STOPPED
            self.IsInstalled = True
            self.on_service_installed()
        except Exception:
            logger.warning("Handling error in service.Install")
            self.Status = Status.ERROR
            raise

    def Uninstall(self):
        logger.info("Uninstalling service %r", self.Name)
        self.Status = Status.UNINSTALLING

        try:
            if self.IsPublished:
                self.tor.stop_hidden_service(self.Address)

            self.TransactionStatus = _("Stopping container")
            self.container.stop()

            self.TransactionStatus = _("Destroying container")
            self.container.destroy()

            self.TransactionStatus = _("Removing service state directory")
            self.remove_state_dir()

            self.remove_options()
            self._address = str()

            self.Status = Status.NOT_INSTALLED
            self.IsInstalled = False
        except Exception:
            logger.warning("Handling error in service.Uninstall")
            self.Status = Status.ERROR
            raise

    def Start(self):
        logger.info("Starting service %r", self.Name)
        try:
            if self.IsRunning and self.IsPublished:
                raise ServiceAlreadyStartedError("Service %r is already started" % self.Name)

            if not self.IsInstalled:
                raise ServiceNotInstalledError("Service %r is not installed" % self.Name)

            self.Status = Status.STARTING

            if not self.container.is_running():
                self.TransactionStatus = _("Starting container")
                self.container.start()
                self.TransactionStatus = _("Bind-mounting config files")
                self.mount()

            self.systemd.start_monitoring()

            if not self.IsRunning:
                self.systemd.restart()

            self.on_systemd_service_started()

            if not self.IsPublished:
                self.TransactionStatus = _("Uploading onion service descriptor")
                self.tor.start_hidden_service(self.private_key.read(), self.Address, self.virtual_port, self.port)

            self.Status = Status.RUNNING
        except Exception as e:
            logger.warning("Handling error in service.Start")
            if DEBUG:
                logger.exception(e)
                input("Paused to allow debugging. Press Enter to continue")

            # Stop the service manually, because calling Stop() would set confusing status messages
            if self.IsRunning:
                self.container.stop()
            if self.IsPublished:
                self.tor.stop_hidden_service(self.Address)
            self.Status = Status.ERROR
            raise

    def Stop(self):
        try:
            logger.info("Stopping service %r", self.Name)
            self.Status = Status.STOPPING

            self.systemd.stop_monitoring()

            if self.container.is_running():
                self.container.stop()

            if self.IsPublished:
                self.tor.stop_hidden_service(self.Address)

            self.Status = Status.STOPPED
        except Exception:
            logger.warning("Handling error in service.Stop")
            self.Status = Status.ERROR
            raise

    def RegenerateAddress(self):
        logger.info("Generating new address")
        was_published = self.IsPublished
        self.Stop()

        self.private_key.create()
        self.Address = self.private_key.derive_onion_address()

        if was_published:
            self.Start()

    # ----- Exported properties ----- #

    @property
    def Options(self) -> List[str]:
        """List of DBus option object paths"""
        return [option.dbus_path for option in self.options_dict.values() if option.registered]

    @property
    def Status(self) -> str:
        return self._status

    @Status.setter
    def Status(self, value: str):
        if self._status == value:
            return
        logger.info("Service %r new Status: %r", self.Name, value)

        changed_properties = dict()
        # Unset the transaction status, because the transaction must be finished if we have a new status
        if self._transaction_status:
            self._transaction_status = changed_properties["TransactionStatus"] = ""

        # Don't accidentally overwrite error status
        if self._status == Status.ERROR and value != Status.UNINSTALLING:
            logger.warning("Not setting new status %r, because the current status is ERROR")
            return

        self._status = changed_properties["Status"] = value
        self.emit_signal("org.freedesktop.DBus.Properties", "PropertiesChanged", changed_properties, "a{ss}")

    @property
    def TransactionStatus(self) -> str:
        return self._transaction_status

    @TransactionStatus.setter
    def TransactionStatus(self, value):
        if self._transaction_status == value:
            return
        logger.debug("Service %r new TransactionStatus: %r", self.Name, value)
        self._transaction_status = value
        self.emit_signal("org.freedesktop.DBus.Properties", "PropertiesChanged",
                         {"TransactionStatus": self.TransactionStatus}, "a{ss}")

    @property
    def TransactionProgress(self) -> int:
        return self._transaction_progress

    @TransactionProgress.setter
    def TransactionProgress(self, value: int):
        if self._transaction_progress == value:
            return
        logger.debug("Service %r new TransactionProgress: %r", self.Name, value)
        self._transaction_progress = value
        self.emit_signal("org.freedesktop.DBus.Properties", "PropertiesChanged",
                         {"TransactionProgress": self.TransactionProgress}, "a{si}")

    @property
    def IsInstalled(self) -> bool:
        return self._is_installed

    @IsInstalled.setter
    def IsInstalled(self, value: bool):
        if self._is_installed == value:
            return
        self._is_installed = value
        self.emit_signal("org.freedesktop.DBus.Properties", "PropertiesChanged",
                         {"IsInstalled": self.IsInstalled}, "a{sb}")

    @property
    def IsRunning(self) -> bool:
        return self.container.is_running() and self.systemd.is_running()

    @property
    def IsPublished(self) -> bool:
        if not self.Address:
            return False
        return self.tor.check_hidden_service_published(self.Address)

    @property
    def Address(self) -> str:
        """
        The hidden service hostname aka onion address of this service.
        :return: onion address
        """
        return self._address

    @Address.setter
    def Address(self, address):
        if address == self._address:
            return
        self._address = address
        self.emit_signal("org.freedesktop.DBus.Properties", "PropertiesChanged", {"Address": self.Address}, "a{ss}")
        self.on_connection_info_changed()

    @property
    @abc.abstractmethod
    def Name(self):
        """The name of the service. This should be the same as the basename of the service's script."""
        return str()

    @property
    def NameForDisplay(self):
        """The name of the service, as it should be displayed in UIs."""
        return self.Name.replace("-", " ").replace("_", " ").title()

    @property
    @abc.abstractmethod
    def Description(self):
        """A short localized description of the service, which can be displayed in UIs."""
        return str()

    @property
    @abc.abstractmethod
    def DescriptionLong(self):
        """A longer description of the service, which can be displayed in UIs."""
        return str()

    @property
    @abc.abstractmethod
    def ClientApplication(self) -> str:
        """The name of the application which can connect to this service."""
        pass

    @property
    def ClientApplicationForDisplay(self) -> str:
        """Name of the application which can connect to this service for displaying in UIs"""
        return self.ClientApplication.replace("-", " ").replace("_", " ").title()

    @property
    def ConnectionInfo(self) -> str:
        """A localized summary of all information required to connect to the service"""
        if not self.Address:
            return str()

        s = self.connection_info_header
        s += _("Application: %s\n") % self.ClientApplicationForDisplay
        s += _("Address: %s:%s") % (self.Address, self.virtual_port)
        return s

    @property
    @abc.abstractmethod
    def Icon(self):
        """Name of the icon to use for this service in a GUI"""
        return self.Name

    DocumentationURL = str()

    # ----- Not exported properties ----- #

    connection_info_format = "1.0"
    connection_info_title = _("Onion Service Connection Information")
    connection_info_header = "=== %s (format %s) ===\n" % (connection_info_title, connection_info_format)

    @property
    @abc.abstractmethod
    def packages(self):
        """Packages needed by this service.
        These will be installed when the service is installed."""
        return list()

    @property
    @abc.abstractmethod
    def systemd_service(self):
        """The name of the service's systemd service"""
        return "%s.service" % self.Name

    @property
    @abc.abstractmethod
    def port(self) -> int:
        """The service's target port (i.e. the port opened by the service on localhost)"""
        pass

    @property
    def virtual_port(self):
        """The port exposed via the onion service, i.e. the port clients have to use
        to connect to the service"""
        if self.IsInstalled and VirtualPort in self.options:
            return self.options_dict[VirtualPort.__name__].Value
        else:
            return self.port

    data_files = list()  # type: List[DataFile]

    options = [
        VirtualPort,
        Persistence,
        Autostart,
        AllowLocalhost,
        AllowLAN,
    ]

    # ----- Not exported functions ----- #

    def update_status(self, status):
        self.Status = status

    def update_transaction_status(self, status):
        self.TransactionStatus = status

    def update_transaction_progress(self, progress):
        self.TransactionProgress = progress

    def on_systemd_service_started(self):
        for option in self.options_dict.values():
            if option.reload_after_service_started:
                option.Reload()

    def on_service_installed(self):
        # Mark the service as successfully installed, by creating the .is_installed file
        self.is_installed_file.touch()

        # The connection info is only available after the service is installed for the first time. This might not
        # be the first time the service is installed, so this might be a "false alarm", but that shouldn't cause
        # any problems.
        self.on_connection_info_changed()

    def on_connection_info_changed(self):
        self.emit_signal("org.freedesktop.DBus.Properties", "PropertiesChanged",
                         {"ConnectionInfo": self.ConnectionInfo}, "a{ss}")

    def configure_top(self):
        """Initial configuration after installing the service.
        This is called *before* the options are initialized. To
        configure the service's options, use configure_options()"""
        pass

    def configure_bottom(self):
        """Configuration that must be done after options are initialized"""
        pass

    def restart_systemd_service(self):
        self.Status = Status.RESTARTING
        self.systemd.restart()
        self.Status = Status.RUNNING

    def initialize_options(self):
        """Initialize options which are not already initialized"""
        for option in self.options:
            self.options_dict[option.__name__] = option(self)
        self.emit_signal("org.freedesktop.DBus.Properties", "PropertiesChanged", {"Options": self.Options}, "a{sao}")

    def remove_options(self):
        """Remove all options from the options dict"""
        logger.debug("Removing options of service %r", self.Name)
        for option in self.options_dict.values():
            option.clean_up()
            option.unregister()
        self.options_dict = dict()

    def restore(self):
        # XXX: Fix me
        logger.info("Restoring service %r", self.Name)
        self.Install()
        self.mount()
        for option in self.options_dict:
            if option != "Persistence":
                self.options_dict[option].apply()

    def create_state_dir(self):
        logger.debug("Creating state directory %s", self.state_dir)
        self.state_dir.mkdir(mode=0o700, parents=True, exist_ok=True)

    def remove_state_dir(self):
        logger.debug("Removing state directory %s", self.state_dir)
        try:
            shutil.rmtree(str(self.state_dir))
        except FileNotFoundError as e:
            logger.warning("Could not remove state directory %s: %s", self.state_dir, e)

    def create_data_files(self):
        logger.debug("Creating data files for %r", self.Name)
        for f in self.data_files:
            # We have to use the UID and GID from the container if the file is a mount point
            if isinstance(f, MountPoint):
                uid = int(self.container.execute_command("id -u %s" % f.owner))
                gid = int(self.container.execute_command("id -g %s" % f.group))
                MountPoint.create(f, uid, gid)
            if isinstance(f, DataFile):
                # Create the data file (only writes the default content if the file was created as a mount point above)
                f.create()

    def expand_data_file_paths(self):
        """Expand the data file paths, which are relative to the service's state directory"""
        logger.debug("Expanding data file paths")
        state_dir = Path(STATE_DIR, self.Name)
        temp_state_dir = Path(TEMP_STATE_DIR, self.Name)
        for f in self.data_files:
            # Remove existing parent state dir (if any)
            if state_dir in f.source.parents:
                f.source = f.source.relative_to(state_dir)
            elif temp_state_dir in f.source.parents:
                f.source = f.source.relative_to(temp_state_dir)
            # Add current state dir as parent
            f.source = Path(self.state_dir, f.source)

    def mount(self):
        """Bind mount config and data files and directories"""
        logger.debug("Mounting bind mounts for %r", self.Name)
        for p in self.mount_points:
            self.container.bind(str(p.source), str(p.target))

    def get_option(self, option_name):
        return self.options_dict[option_name].Value

    def set_option(self, option_name, value):
        self.options_dict[option_name].Value = value

    def reset_option(self, option_name):
        option = self.options_dict[option_name]
        option.Value = option.Default

    def set_initial_status(self):
        if self.state_dir.exists() and not self.is_installed_file.exists():
            logger.warning("Directory '%s' exists but installation was not completed. Removing directory.",
                           self.state_dir)
            self.remove_state_dir()

        if not self.IsInstalled:
            if self.IsPublished or self.IsRunning:
                self.Stop()
            self.Status = Status.NOT_INSTALLED
        elif self.IsInstalled and not self.IsRunning and not self.IsPublished:
            self.Status = Status.STOPPED
        elif self.IsInstalled and self.IsRunning and self.IsPublished:
            self.Status = Status.RUNNING
        else:
            # If the service is in an invalid status during initialization,
            # we stop the service and allow the user to try restarting it.
            logger.warning("Service %r is in an invalid state: IsInstalled: %r, IsRunning: %r, IsPublished: %r",
                           self.Name, self.IsInstalled, self.IsRunning, self.IsPublished)
            self.Stop()

    @contextmanager
    def prevent_service_autostart(self):
        policy_rc = Path(self.container.machine_dir, "usr/sbin/policy-rc.d")
        # Forbid all invoke-rc.d actions
        # See https://people.debian.org/~hmh/invokerc.d-policyrc.d-specification.txt
        policy_rc.write_text("exit 101")
        policy_rc.chmod(0o755)
        try:
            yield
        finally:
            policy_rc.unlink()
