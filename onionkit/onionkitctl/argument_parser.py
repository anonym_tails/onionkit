import argparse
import sys
from os import path

from pydbus import SystemBus

from onionkit import BUS_NAME, SERVICES_PATH


class HelpfulParser(argparse.ArgumentParser):
    """Argument parser that prints the help message on error"""
    def error(self, message):
        self.print_help()
        sys.stderr.write('\nerror: %s\n' % message)
        sys.exit(2)


class ServiceAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        """Verify that the specified service is exported by OnionKit"""
        try:
            SystemBus().get(BUS_NAME, path.join(SERVICES_PATH, values))
        except KeyError:
            parser.error("Could not find service %r" % values)

        namespace.service = values


class OptionAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        """Verify that the specified option is exported by OnionKit"""

        def verify_option_name(name):
            try:
                SystemBus().get(BUS_NAME, path.join(SERVICES_PATH, namespace.service, name))
            except KeyError:
                parser.error("Could not find option %r for service %r" % (name, namespace.service))

        if self.dest == "OPTION":
            namespace.options = list()
            for option_name in values:
                verify_option_name(option_name)
                namespace.options.append(option_name)

        if self.dest == "ASSIGNMENT":
            namespace.assignments = list()
            for assignment in values:
                try:
                    option_name, value = assignment.split("=")
                    verify_option_name(option_name)
                    namespace.assignments.append((option_name, value))
                except ValueError:
                    parser.error("Invalid assignment %r" % assignment)


class CommandParser(argparse.ArgumentParser):

    formatter_class = argparse.RawTextHelpFormatter

    def add_service_command(self, command_name, *args, **kwargs):
        subparser = self.subparsers.add_parser(command_name, *args, **kwargs)
        subparser.add_argument(dest="SERVICE", action=ServiceAction, help="Name of the service")
        return subparser

    def __init__(self, *args, **kwargs):
        kwargs["formatter_class"] = self.formatter_class
        super().__init__(*args, **kwargs)
        self.add_argument("--verbose", "-v", action="count")
        self.add_argument("--log-file")

        self.subparsers = self.add_subparsers(dest="command", parser_class=HelpfulParser)

        # Add general commands
        self.subparsers.add_parser("list",           help="Print list of available services")
        self.subparsers.add_parser("list-installed", help="Print list of installed services")
        self.subparsers.add_parser("list-running",   help="Print list of running services")
        self.subparsers.add_parser("list-published", help="Print list of published services")
        self.subparsers.add_parser("restore",        help="Install packages and restore files of services which have the 'persistence' option set to True")
        self.subparsers.add_parser("autostart",      help="Enable the services which have the 'autostart' option set to True")

        # Add service commands
        self.add_service_command("show",            help="Print properties the service")
        self.add_service_command("connection-info", help="Print connection information required by clients to connect to the service")
        self.add_service_command("status",          help="Print whether the service is installed and running")
        self.add_service_command("install",         help="Install the service")
        self.add_service_command("uninstall",       help="Uninstall the service")
        self.add_service_command("start",           help="Starts the service")
        self.add_service_command("stop",            help="Stops the service")

        # Add option commands
        subparser = self.add_service_command("get-option",   help="Print the current value of an option.\nExample: onionkitctl get-option mumble AllowLocalhost")
        subparser.add_argument(dest="OPTION", nargs="+", action=OptionAction, help="Option name. Example: VirtualPort")

        subparser = self.add_service_command("set-option",   help="Set an option. If the service is running, the option will be applied immediately, and, if necessary, the service will be restarted.\nExample: onionkitctl set-option mumble ServerPassword=\"foo\"")
        subparser.add_argument(dest="ASSIGNMENT", nargs="+", action=OptionAction, help="Option name and value. Example: VirtualPort=12345")

    def parse_args(self, **kwargs):
        args = super().parse_args(**kwargs)

        if not any(vars(args).values()):
            self.print_help()
            self.exit()

        return args
