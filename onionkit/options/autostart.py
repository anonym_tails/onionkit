from onionkit import _
from onionkit.option import OnionServiceOption


class Autostart(OnionServiceOption):
    NameForDisplay = _("Autostart")
    Description = _("Start service automatically when onionkit is started")
    Group = _("General")
    Default = False
    type = bool
    is_applied_without_restarting = True
