import sh
from pathlib import Path
from logging import getLogger

from onionkit import _
from onionkit.option import OnionServiceOption

from onionkit import STATE_DIR, TEMP_STATE_DIR


logger = getLogger(__name__)


class Persistence(OnionServiceOption):
    NameForDisplay = _("Persistence")
    Description = _("Store service configuration and data on the persistent volume")
    Group = _("General")
    Default = False
    type = bool
    is_applied_without_restarting = True

    def load(self):
        return self.is_persistent()

    def store(self):
        pass

    def apply(self):
        super().apply()
        if self.Value == self.is_persistent():
            logger.debug("Current value of option '%s.%s' is already applied", self.service.Name, self.Name)
            return
        if self.Value:
            self.make_persistent()
        else:
            self.remove_persistence()

    def make_persistent(self):
        logger.info("Making service %r persistent", self.service.Name)

        # Ensure service is stopped
        was_running = self.service.IsRunning
        self.service.Stop()

        # Unmount bind mounts, because we are going to move the source files
        self.service.unmount()

        # Move the state directory to the persistent filesystem
        state_dir = Path(STATE_DIR, self.service.Name)
        self.move(self.service.state_dir, state_dir)
        self.service.state_dir = state_dir
        self.service.expand_data_file_paths()

        # Mount bind mounts again
        self.service.bind()

        # Start service again if it was running
        if was_running:
            self.service.Start()

    def remove_persistence(self):
        logger.info("Removing persistence of service %r", self.service.Name)

        # Ensure service is stopped
        was_running = self.service.IsRunning
        self.service.Stop()

        # Unmount bind mounts, because we are going to move the source files
        self.service.unmount()

        # Move the state directory to the temporary filesystem
        tmp_state_dir = Path(TEMP_STATE_DIR, self.service.Name)
        self.move(self.service.state_dir, tmp_state_dir)
        self.service.state_dir = tmp_state_dir
        self.service.expand_data_file_paths()

        # Mount bind mounts again
        self.service.bind()

        # Start service again if it was running
        if was_running:
            self.service.Start()

    def is_persistent(self):
        return Path(STATE_DIR) in self.service.state_dir.parents

    @staticmethod
    def move(src: Path, dest: Path):
        logger.debug("Moving %r to %r", src, dest)
        if dest.exists():
            raise FileExistsError("Couldn't move %r to %r, destination %r already exists" %
                                  (src, dest, dest))
        sh.mv(str(src), str(dest))
