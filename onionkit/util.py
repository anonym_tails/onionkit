from logging import getLogger
import threading
import fcntl
from contextlib import contextmanager
from typing import TextIO
from pathlib import Path

from gi.repository import GLib


logger = getLogger(__name__)


def process_mainloop_events():
    context = GLib.MainLoop().get_context()
    while context.pending():
        context.iteration()


@contextmanager
def open_locked(path: Path, *args, **kwargs) -> TextIO:
    with path.open(*args, **kwargs) as f:
        try:
            logger.log(5, "Acquiring file lock on %s", path)
            fcntl.flock(f, fcntl.LOCK_EX)
            logger.log(5, "Acquired file lock on %s", path)

            yield f

        finally:
            logger.log(5, "Releasing file lock on %s", path)
            fcntl.flock(f, fcntl.LOCK_UN)
